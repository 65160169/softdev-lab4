/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevlab4;

/**
 *
 * @author Asus
 */
public class Player {
    private char symbol;
    private int wincount,losecount,drawcount;
    
    public Player(char symbol){
        this.symbol = symbol;
        this.wincount = 0;
        this.losecount = 0;
        this.drawcount = 0;
    }
    
    public char getsymbol(){
        return symbol;
    }
    
    public void win(){
        wincount++;
    }
    
    public void lose(){
        losecount++;
    }
    
    public void drawcount(){
        drawcount++;
    }

    public int getWincount() {
        return wincount;
    }

    public int getLosecount() {
        return losecount;
    }

    public int getDrawcount() {
        return drawcount;
    }
    
    
}
