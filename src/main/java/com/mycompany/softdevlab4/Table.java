/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevlab4;

/**
 *
 * @author Asus
 */
public class Table {
  private Player player1;
  private Player player2;
  private Player currentplayer;
  private char[][] table = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
  
  public Table(Player player1,Player player2){
      this.player1 = player1;
      this.player2 = player2;
      this.currentplayer = player1;
  }
  
  public char[][] getTable(){
      return table;
  }
  
  public Player getcurentplayer(){
      return currentplayer;
  }
  
  public void setrowcol(int row,int col){
      table[row][col] = currentplayer.getsymbol();
  }
  
  public void switchox(){
      if(player1==currentplayer){
          currentplayer = player2;
      }else{
          currentplayer = player1;
      }
  }
  
  public boolean checkwin(char currentplayer){
      for(int row=0;row<3;row++){
          if(checkrow(currentplayer,row)){
              return true;
          }
      }
      for(int col=0;col<3;col++){
          if(checkcol(currentplayer,col)){
              return true;
          }
      }
      if(checkslash(currentplayer)){
          return true;
      }
      if(checkbackslash(currentplayer)){
          return true;     
      }
      return false;
  }
  
  public boolean checkdraw(){
      return table[0][0] != '_' && table[0][1] != '_' && table[0][2] != '_' &&
             table[1][0] != '_' && table[1][1] != '_' && table[1][2] != '_' &&
             table[2][0] != '_' && table[2][1] != '_' && table[2][2] != '_';
  }
  
  private boolean checkrow(char currentplayer,int row){
      return table[row][0]==currentplayer && table[row][1]==currentplayer && table[row][2]==currentplayer;
  }
  private boolean checkcol(char currentplayer,int col){
      return table[0][col] == currentplayer && table[1][col] == currentplayer && table[2][col] == currentplayer;
   }
  private boolean checkslash(char currentplayer){
      return table[0][2] == currentplayer && table[1][1] == currentplayer && table[2][0] == currentplayer;
  }
  private boolean checkbackslash(char currentplayer){
      return table[0][0] == currentplayer && table[1][1] == currentplayer && table[2][2] == currentplayer;
  }
  
}
