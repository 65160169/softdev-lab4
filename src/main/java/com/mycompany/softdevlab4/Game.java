/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.softdevlab4;

/**
 *
 * @author Thanaphat ketsani 65160169
 */
import java.util.Scanner;
public class Game {
    private Player player1;
    private Player player2;
    private Table table;
    private static boolean status = true; 
    
    public Game(){
        this.player1 = new Player('X');
        this.player2 = new Player('O');
    }
    
    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }
    
    private void showwelcome(){
        System.out.println("Welcome To My OX game");
    }
    
    private void showtable(){
        char[][] t = table.getTable();
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(t[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public void newGame(){
        this.table = new Table(player1,player2);
    }
    
    private void showturn(){
        System.out.print("Turn "+table.getcurentplayer().getsymbol()+" please input row,col: ");
    }
    
    private void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        showturn();
        int row = kb.nextInt(); int col = kb.nextInt();
        if(table.getTable()[row][col] != '_'){
            inputRowCol();
        }else{
            table.setrowcol(row, col);
        }
    }
    
    private void askContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.println("Continue?(y/n): ");
        char yn = kb.next().charAt(0);
        if(yn=='y'){
            status = true;
            System.out.println("New Game Start");
            newGame();
        }else{
            status = false;
            System.out.println("End Game . . .");
            newGame();
        }
    }
    
    private void showstats(){
        System.out.println(table.getcurentplayer().getsymbol()+" Stats:");;
        System.out.println("Win: "+table.getcurentplayer().getWincount());
        System.out.println("lose: "+table.getcurentplayer().getLosecount());
        table.switchox();
        System.out.println();
        System.out.println(table.getcurentplayer().getsymbol()+" Stats:");;
        System.out.println("Win: "+table.getcurentplayer().getWincount());
        System.out.println("lose: "+table.getcurentplayer().getLosecount());
        System.out.println();
        System.out.println("Draw: "+table.getcurentplayer().getDrawcount());
    }
    
    public void play(){
        showwelcome();
        newGame();
        while(status==true){
            showtable();
            inputRowCol();
            if(table.checkwin(table.getcurentplayer().getsymbol())){
                System.out.println("The Winner is "+table.getcurentplayer().getsymbol());
                table.getcurentplayer().win();
                table.switchox();
                table.getcurentplayer().lose();
                showstats();
                askContinue();
            }
            if(table.checkdraw()){
                System.out.println("<<<< DRAW >>>>");
                table.getcurentplayer().drawcount();
                showstats();
                askContinue();
            }
            table.switchox();
        }
    }
}
